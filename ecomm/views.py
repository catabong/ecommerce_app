from django.http.response import JsonResponse
import json
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from authentication.forms import customerForm
from ecomm.models import *
# Create your views here.



def index(request):
    products = Product.objects.all()
    brands = Brand.objects.all()
    cartitem = CartItem.objects.all()
    history = History.objects.all()

    paginator = Paginator(history, 4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'index.html',{'products':products,'brands': brands, 'cartitem': cartitem, 'page_obj':page_obj, 'paginator':paginator})

def cart(request):
    if request.user.is_authenticated:

       customer = Customer.objects.get(user=request.user)
       cart, created= Cart.objects.get_or_create(customer=customer)
        
       cartItems = CartItem.objects.filter(cart=cart)
    
       return render(request, 'cart.html',{'cartItems':cartItems})
    else:
        
        cartCookies = json.loads(request.COOKIES['cart'])
        
        cartItems = []
        cart = {'orderTotal':0, 'orderQuan':0}

        if cartCookies:
            cartQuan = 0
            cartTotal = 0
            for cookie in cartCookies:
                quantity = cartCookies[cookie]['quantity']
                product = Product.objects.get(id = cookie)
                itemTotal = quantity * product.current_price
                orderitem = {'itemTotal':itemTotal,
                'quantity': quantity,
                    'product':
                {'id':product.id,
                'title':product.title,
                'imageUrl':product.imageUrl,
                }
                }
                
                cartQuan += quantity

                cartTotal += itemTotal
                cartItems.append(orderitem)

                cart = {'orderTotal':cartTotal,     'orderQuan':cartQuan}
        
        
        return render(request, 'cart.html',     {'cartItems': cartItems, 'cart':cart})
   
        
def product(request, product_id):
    product = Product.objects.get(pk=product_id)
    colors = Color.objects.filter(product=product)
   
    return render(request, 'product.html', {'product': product, 'colors':colors})


def profile(request):
    return render(request, 'profile.html',{})

def checkout(request):

    customer = Customer.objects.get(user=request.user)
    cart, created= Cart.objects.get_or_create(customer=customer)
        
    cartItems = CartItem.objects.filter(cart=cart)
    return render(request, 'checkout.html', {'cartItems': cartItems})

def wishlist(request):
    if request.user.is_authenticated:
        customer = request.user.customer
        wishlist, created= Wishlist.objects.get_or_create(customer=customer)
        wishitems = Wishitem.objects.filter(wishlist=wishlist)
    else:
        cookies = json.loads(request.COOKIES['wish'])
        wishitems = []
        for cookie in cookies:
            product = Product.objects.get(id = cookie)
            wishitem = {'product':{'id':product.id,
                        'title': product.title,
                        'imageUrl': product.imageUrl}}
            wishitems.append(wishitem)
        print(wishitems)
            
                   
    return render(request, 'wishlist.html',{'wishitems':wishitems})

def updateCart(request):
    data = json.loads(request.body)
    productId = data['productId']
    action = data['action']
    
    customer = request.user.customer
    product = Product.objects.get(id=productId)
    cart = Cart.objects.get(customer=customer)
    cartItems, created = CartItem.objects.get_or_create(product=product, cart=cart)

 
    if action =='move':
        wishitem = Wishitem.objects.get(product=product)
        wishitem.delete()

    if action == 'add':
        cartItems.quantity = (cartItems.quantity + 1)

    elif action == 'remove':
        if cartItems.quantity == 1:
            return
        cartItems.quantity = (cartItems.quantity - 1)

    cartItems.save()

    if action == 'delete':
        cartItems.delete()
   
    return JsonResponse('Item added to cart',safe=False)

def updateWishList(request):
    
    data = json.loads(request.body)
    productId = data['productId']
    action = data['action']

    customer = request.user.customer
    wishlist = Wishlist.objects.get(customer=customer)
    product = Product.objects.get(id=productId)
    wishitem, created = Wishitem.objects.get_or_create(product=product, wishlist=wishlist)

    wishitem.save()

    if action == 'delete':
        wishitem.delete()

    return JsonResponse('Item added to wishlist',safe=False)

def recentlyViewed(request):
    data = json.loads(request.body)
    
    action = data['action']

    if action =='clear':
        print('true')
        histories = History.objects.all()
        for history in histories:
            history.delete()
            
        return

    productId = data['productId']

    customer = request.user.customer
    product = Product.objects.get(id=productId)

    history, created = History.objects.get_or_create(customer=customer, product=product)

    history.save()

    if action == 'remove':
        print('true')
        history.delete()

    

    return JsonResponse('Item added to history',safe=False)

def search(request):
    
    searched = request.POST['search']
    results = Product.objects.filter        (description__contains=searched)

    return render(request, 'search.html',         {'searched':searched, 'results':results })


def profile(request):
    form = customerForm
    return render(request, 'profile.html', {'form':form})