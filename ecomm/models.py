from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE, SET_NULL
from django.db.models.fields import BooleanField, DateTimeField
from django.db.models.fields.related import ForeignKey

# Create your models here.

class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=CASCADE)
    firstName = models.CharField(max_length=50, null=True,blank=True)
    lastName = models.CharField(max_length=50, null=True,blank=True)
    email = models.EmailField(null=True,blank=True)
    profile = models.ImageField(default='user.png',null=True)
    phone = models.CharField(max_length=11, null=True)
    
    def __str__(self):
        return self.user.username

class Brand(models.Model):
    title = models.TextField(max_length=12, blank=False, null=True)
    logo = models.ImageField(null=True)

    def __str__(self):
        return self.title


class Product(models.Model):
    
    title = models.CharField(max_length=50, blank=True)
    discount_price = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank = True)
    current_price = models.DecimalField(max_digits=20, decimal_places=2, null=True)
    image = models.ImageField(blank=False)
    ingredients = models.TextField()
    description = models.TextField()
    available = models.IntegerField()

    def __str__(self):
        return self.title

    @property
    def imageUrl(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url

    @property
    def colortitle(self):
        color = self.color_set.all()
        return color

class Color(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True)
    image = models.ImageField(blank=True, null=True)
    product = models.ForeignKey(Product, null=True, on_delete=CASCADE)

    def __str__(self):
        return self.product.title + ' in ' + self.title

class Productmedia(models.Model):
    product = models.ForeignKey(Product, on_delete=CASCADE, null=True)
    image = models.ImageField(blank=False)

    @property
    def imageUrl(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url

    def __str__(self):
        return self.product.title


class Wishlist(models.Model):
    customer = models.ForeignKey(Customer, on_delete=CASCADE)

    def __str__(self):
       return str(self.customer) + "'s" +' order'

class Wishitem(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=CASCADE)
    wishlist = models.ForeignKey(Wishlist, null=True, on_delete=CASCADE)

    def __str__(self):
        return self.product.title


class Cart(models.Model):
    customer = models.ForeignKey(Customer, on_delete=CASCADE, null=True)
    
    @property
    def cartTotal(self):
       cartItems = self.cartitem_set.all()
       total = sum([cartItem.itemTotal for cartItem in cartItems])
       return total
    
    @property
    def cartQuan(self):
        cartquan = self.cartitem_set.all()
        total = sum(quan.quantity for quan in cartquan)
        return total

    def __str__(self):
        return str(self.customer) + "'s" +' order'


class CartItem(models.Model):
   cart = models.ForeignKey(Cart, on_delete=CASCADE, null=True)
   product = models.ForeignKey(Product, null=True, on_delete=CASCADE)
   quantity = models.PositiveIntegerField(default=1, blank= False)


   def __str__(self):
        return self.product.title

   @property
   def itemTotal(self):
       total = self.product.current_price * self.quantity
       return total

class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=CASCADE, null=True)

class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=CASCADE, null=True)
    cartitem = models.ManyToManyField(CartItem)

    def __str__(self):
        return self.cartitem.product.title

class History(models.Model):
    customer = models.ForeignKey(Customer, on_delete=CASCADE, null=True)

    product = models.ForeignKey(Product, null=True, on_delete=CASCADE)

    

'''class Shipping(models.Model):
    customer = models.ManyToManyField(null=True)
    country = models.CharField(max_length=100, null = True)
    state = models.CharField(max_length=100, null = True)
    city = models.CharField(max_length=100, null = True)
    zipcode = models.IntegerField(max_length=100, null = True)
    address = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.address'''
