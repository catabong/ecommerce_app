from ecomm.views import wishlist
from .models import *
import json
def userBags(request):
    cartQuan = 0

    if request.user.is_authenticated:
       
       customer = Customer.objects.get(user=request.user)
       cart, created= Cart.objects.get_or_create(customer=customer)
       wishlist = Wishlist.objects.get(customer=customer)
       wishitem = Wishitem.objects.filter(wishlist=wishlist)

       return {'cart':cart, 'wishitem':wishitem}
       
    else:
        try:
            cartCookies = json.loads(request.COOKIES    ['cart'])

            wishitem = json.loads(request.COOKIES['wish'])

        except:
            cartCookies={}
            wishitem=[]

        for cookie in cartCookies:
            quantity = cartCookies[cookie]['quantity']

            cartQuan += quantity
            
        
        order = {'cartQuan':cartQuan}
        return {'order': order, 'wishitem':wishitem}

        
