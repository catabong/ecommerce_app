from django import forms
from ecomm.models import Shipping

class Formpage(forms.ModelForm):
    class Meta:
        model = Shipping
        fields = ('customer','country', 'state', 'city', 'zipcode', 'address')