from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('cart/', views.cart, name = 'cart'),
    path('product/<int:product_id>', views.product, name = 'product'),
    path('profile/', views.profile, name = 'profile'),
    path('wishlist/', views.wishlist, name = 'wishlist'),
    path('searched/', views.search, name = 'search'),
    path('checkout/', views.checkout, name = 'checkout'),
    path('add-to-cart/', views.updateCart, name = 'add-to-cart'),
    path('add-to-wish/', views.updateWishList, name = 'add-to-wish'),
    path('add-to-history/', views.recentlyViewed, name = 'add-to-history'),
]
