from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm

from ecomm.models import Customer

class registerForm(UserCreationForm):

    class Meta:
        model = User
        fields = [
             'first_name',
             'last_name',
             'username',
             'password1',
             'password2'
             ]

class customerForm(ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'
        exclude = ['user']
