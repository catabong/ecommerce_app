from django.shortcuts import render, redirect
from .forms import *
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def auth(request):
    form = registerForm
    if request.method == 'POST':
        if request.POST.get('submit') == 'login':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request,     username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/')
            else:
                messages.success(request, ('An error   occured. Please fill in your details   correctly.'))
                return redirect('/auth')

        elif request.POST.get('submit') == 'signup':
            form = registerForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data ['username']
                password = form.cleaned_data ['password1']
                authenticate(request,  username=username,     password=password)
                return redirect('/auth')
    
            form = registerForm()
            
           
           
    return render(request, 'login.html', {'form':form})


def signout(request):
    logout(request)
    return redirect('/auth')

