var updateCart= document.querySelectorAll('.updatecart');
console.log(updateCart)

updateCart.forEach(cartSubset => { 
    
    cartSubset.addEventListener('click',function(){
    
    var productId = this.dataset.product
    var action = this.dataset.action
    console.log('productId:', productId, 'action:', action, 'User:', user);
     
    if (user === 'AnonymousUser'){
        console.log(true)
        if(action == 'add'){
            if (cart[productId] == undefined){
                cart[productId] = {'quantity':1}
            }
            else {
                
                cart[productId]['quantity'] += 1
                document.cookie = 'cart=' + JSON.stringify(cart) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()
            }
                    
        }
    
        if(action == 'remove') {
            if (cart[productId]['quantity'] <= 0){
                delete cart[productId]

                document.cookie = 'cart=' + JSON.stringify(cart) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()
            }
            else
            
            {cart[productId]['quantity'] -= 1}
            document.cookie = 'cart=' + JSON.stringify(cart) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()

        }
    
        if(action == 'move'){
            index = wish.indexOf(productId)
            wish.splice(index)

           document.cookie = 'wish=' + JSON.stringify(wish) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()
        
           cart[productId]={'quantity':1}
           document.cookie = 'cart=' + JSON.stringify(cart) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()}
        
        if(action == 'delete'){
            delete cart[productId] 
            console.log(cart)

            document.cookie = 'cart=' + JSON.stringify(cart) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()
        }}

    
    else {
        updatecart(productId, action);
          }

})

})

// xhr.setRequestHeader ('X-CSRFToken', csrftoken
// );

function updatecart(productId, action){
    /*var xhr = new XMLHttpRequest();
    console.log(xhr)
    
    xhr.open('POST', '/add-to-cart/', true);

    xhr.setRequestHeader ('X-CSRFToken', csrftoken
    );
    

    xhr.onload = function () {
        if(this.status == 200){
            console.log('true')
        }
        else {console.log('false')}
    }


    var body = {'productId':productId, 'action':action}
    xhr.send(JSON.stringify(body))*/

    var body = {'productId':productId, 'action':action};
    $.ajax({
        headers:{'X-CSRFToken':csrftoken},
        type:'POST',
        url:'/add-to-cart/',
        data:JSON.stringify(body),
        success: function(){
        
        $('#wish-total-wrapper' ).load(' #wish-total');

        $('#cart-total-wrapper').load(' #cart-total');

        $('#checkbox-outer-wrap').load(' #checkbox-wrap');

        $('#row1').load(' #row1-content');

        $(`#item-price-wrapper-${productId}`).load(` #th1-${productId}`);

        $(`#product-quantity-wrapper-${productId}`).load(` #product-quantity-${productId}`);

        $(`#wish-content-${productId}`).load(` #wish-content-${productId} >*`);

        if(action=='delete'){
            $(`#row-row-${productId}`).load(` #row-row-${productId} >*`);
            }

        }   
    })
}


var updateWishList= document.getElementsByClassName('updatewish');

for (let i = 0; i < updateWishList.length; i++){

    updateWishList[i].addEventListener('click',function(){


        var productId = this.dataset.product
        var action = this.dataset.action
        console.log('productId:', productId, 'action:', action, 'User:', user);
         
        if (user === 'AnonymousUser'){
            guestUserWishlist(productId, action)
        } 
        
        else {
            updateWish(productId, action)
        }
    })

}

function guestUserWishlist(productId, action){

    if (action == 'delete'){
        index = wish.indexOf(productId)
        wish.splice(index)

        document.cookie = 'wish=' + JSON.stringify(wish) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()
        
        
    }

    else {
        if (wish.includes(productId)) return

        else { wish.push(productId)
               document.cookie = 'wish=' + JSON.stringify(wish) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()}
    }

}


function updateWish(productId, action){

    var body = {'productId':productId, 'action':action}
    $.ajax({
        headers:{'X-CSRFToken':csrftoken},
        type:'POST',
        url:'/add-to-wish/',
        data: JSON.stringify(body),
        success: function(){
            $( '#wish-total-wrapper' ).load(' #wish-total');

            $(`#wish-content-${productId}`).load(` #wish-content-${productId} >*`);
            
        }
    })

    if(action=='add'){

        if (userCart.includes(productId)) return

        else {userCart.push(productId)
            document.cookie = 'userCart=' + JSON.stringify(userCart) + ';domain=;path=/' + '; expires=' + new Date(9999, 8, 15).toUTCString()}
    }
    
}

//var userCart = JSON.parse(getCookie('userCart'));


var products = document.querySelectorAll('.products');
console.log(products)

products.forEach(product => {
        product.addEventListener('click', () => {

        var productId = product.dataset.productid

        var action = product.dataset.action
        console.log('action:',action, productId)

        var body = {'productId':productId, 'action':action}
        $.ajax({
        headers:{'X-CSRFToken':csrftoken},
        type:'POST',
        url:'/add-to-history/',
        data: JSON.stringify(body),
        success: function(){
            $( '#recent-outer' ).load(' #recently-viewed')

            if(action == 'remove'){
            $( `#recent-${productId}` ).load(` #recently-viewed-${productId}`)}
            
        }
    })
        }
)

})


document.querySelectorAll('.paginator').forEach(page => {
    page.addEventListener('click', () => {
         location.href = page.id
        
        $.ajax({
            headers:{'X-CSRFToken':csrftoken},
            type:'POST',
            url: location.href,
            data: '',
            success: function(){
                $( '#recent-outer' ).load(' #recently-viewed')}

    })
})

    })